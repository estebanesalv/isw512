
function guardar_Datos() {

    let persona = {
        nombre: document.getElementById("txt_nombre").value,
        apellido: document.getElementById("txt_apellido").value,
        celular: document.getElementById("txt_celular").value
    };
    
    localStorage.setItem (persona.nombre, JSON.stringify(persona));
    obtener_localstorage();
}

function obtener_localstorage(){
    let persona = JSON.parse(localStorage.getItem("persona"));
    console.log(persona);
}
